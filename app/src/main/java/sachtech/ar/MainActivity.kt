package sachtech.ar

import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.google.ar.core.Anchor
import com.google.ar.sceneform.AnchorNode
import com.google.ar.sceneform.rendering.ModelRenderable
import com.google.ar.sceneform.ux.ArFragment
import com.google.ar.sceneform.ux.TransformableNode
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        (model_fragment as ArFragment).setOnTapArPlaneListener { hitResult, plane, motionEvent ->
            val anchor = hitResult.createAnchor()
            ModelRenderable.builder()
                .setSource(this@MainActivity, Uri.parse("human_female.sfb"))
                .build()
                .thenAccept { modelRenderable -> addModelToScreen(anchor, modelRenderable) }
                .exceptionally { throwable ->
                    Log.e("sg", "Unable to load RenderAble.", throwable)
                    Toast.makeText(this@MainActivity, "error:" + throwable.localizedMessage,
                        Toast.LENGTH_LONG).show()
                    null
                }
            val action = motionEvent.action
            Toast.makeText(this@MainActivity,action.toString(),Toast.LENGTH_LONG).show()
        }
    }


    private fun addModelToScreen(anchor: Anchor?, modelRenderable: ModelRenderable?) {
        val anchorNode = AnchorNode(anchor)
        val transformableNode = TransformableNode((model_fragment as ArFragment).transformationSystem)
        transformableNode.setParent(anchorNode)
        transformableNode.renderable = modelRenderable
        (model_fragment as ArFragment).arSceneView.scene.addChild(anchorNode)
        transformableNode.select()
    }
}

